---
title: "Something About Me"
draft: false
---

<div class="bg-white">
  <div class="relative bg-gray-800 pb-32">
    <div class="absolute inset-0">
      <img class="h-full w-full object-cover" src="https://images.unsplash.com/photo-1525130413817-d45c1d127c42?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=60&&sat=-100"alt="">
      <div class="absolute inset-0 bg-gray-800 mix-blend-multiply" aria-hidden="true"></div>
    </div>
    <div class="relative mx-auto max-w-7xl py-12 px-10 sm:py-12 lg:px-8">
      <div class="mb-8 h-12">
        <img class="object-contain h-10" src="images/gitlab-logo-200.png"/>
      </div>
      <h1 class="text-4xl font-bold tracking-tight text-white md:text-5xl lg:text-7xl">Kris Lefebvre</h1>
      <p class="mt-3 max-w-3xl text-5xl text-gray-300">Enterprise Sales Leader</p>
    </div>
  </div>
  <section class="relative z-10 mx-auto -mt-40 max-w-7xl px-6 pb-32 lg:px-8">
    <div class="flex items-center justify-center space-x-2">
      <img src="images/klefebvr.jpg" alt="klefebvr" class="relative w-72 h-72 z-30 rounded-full object-cover border-sky-400 ring-8">
    </div>
    <div class="bg-white">
      <div class="mx-auto max-w-7xl px-6 sm:pt-10 lg:py-40 lg:px-8">
        <div class="lg:grid lg:grid-cols-12 lg:gap-8">
          <div class="lg:col-span-5">
            <h2 class="text-4xl font-semibold leading-7 text-gray-900">About Me</h2>
            <p class="mt-2 text-2xl leading-7 text-gray-600">Hello and thanks for stopping by! My name is Kris and I am the enterprise sales leader for the Southeast here at GitLab.  After serving 8 years in the United States Army, I transitioned out and began my career in technology and as such have worn may hats in my 25 years in the industry.  What I love the most about my job is getting to meet and interact with new people in addition to helping customers indentify and achieve their corporate outcomes. Please feel free to reach out and connect; 
            klefebvre@gitlab  
            +1-203-581-4661</p>
          </div>
          <div class="mt-10 lg:col-span-7 lg:mt-0">
            <dl class="space-y-10 mb-8">
              <div>
                <dt class="text-4xl font-semibold leading-7 text-gray-900">Why GitLab</dt>
                <dd class="mt-2 text-2xl leading-7 text-gray-600">At GitLab we understand time is money! Software and speed are critical to our clients' ability to meet the demands of increasing revenue and surpassing the competition. GitLab provides an innovative approach to modernize your software supply chain by securely providing a singular and comprehensive view into all the associated software build and publication processes, which allows our clients' to turn strategy into software reality.  Take your big idea and turn it into a competitive advantage today!
                </dd>
              </div>
            </dl>
            <dl class="space-y-10 mb-8">
              <div>
                <dt class="text-4xl font-semibold leading-7 text-gray-900">Customer Collateral</dt>
                <dd class="mt-2 text-2xl leading-7 text-gray-600" src= "https://drive.google.com/drive/folders/125NmtbDRGaP9nEtZWIA2aJfZFK73mgdK"/dd>
              </div>
            </dl>
            <dl class="space-y-10 mb-8">
              <div>
                <dt class="text-4xl font-semibold leading-7 text-gray-900">Upcoming Events</dt>
                <dd class="mt-2 text-2xl leading-7 text-gray-600">March 16, 2023 - Gitlab and Google Cloud March Madness Watch Party in Atlanta </dd>
              </div>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- This example requires Tailwind CSS v3.0+ -->


  
</div>




